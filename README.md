# Nix Media

## Docker Deployment

To deploy the app on a server, take the following steps -

 - `git clone https://NickR49@bitbucket.org/NickR49/nixmedia-deploy.git`
 - `docker login -u _json_key --password-stdin https://asia.gcr.io < nixmedia*.json`
 - `docker-compose up -d`
 
 Done!
 